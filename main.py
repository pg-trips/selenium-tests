from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

from time import sleep


# Set options for not prompting DevTools information
options = Options()
options.add_experimental_option("excludeSwitches", ["enable-logging"])

print("Testing started")
driver = webdriver.Chrome(options=options)
driver.get("http://localhost:3000/")
sleep(1)

# Find element using element's id attribute
driver.find_element(By.CLASS_NAME, "mdi-account").click()
driver.find_element(By.XPATH, "//input[@test-name='login-username-field']").send_keys("admin")
driver.find_element(By.XPATH, "//input[@test-name='login-password-field']").send_keys("admin")
driver.find_element(By.XPATH, "//span[text()=' Zaloguj ']").click()
driver.find_element(By.XPATH, "/html/body/div/div/div/main/div[2]/div[1]/div[3]/div[1]/div/div/div/div[3]/input").send_keys("Egipt")
driver.find_element(By.XPATH, "/html/body/div/div/div/main/div[2]/div[1]/div[3]/div[3]/div/div/div/div[3]/input").send_keys("Warszawa")
driver.find_element(By.XPATH, "/html/body/div/div/div/main/div[2]/div[1]/div[3]/div[4]").click()
driver.find_element(By.XPATH, "/html/body/div[2]/div/div[2]/div/div[3]/div[1]/div[1]/div[1]/div/div[3]/input").send_keys("\b2")
driver.find_element(By.XPATH, "//span[text()=' Zamknij ']").click()
driver.find_element(By.XPATH, "//span[text()=' Szukaj ']").click()
sleep(1)
driver.find_element(By.XPATH, "//span[text()=' Sprawdź ofertę ']").click()
sleep(1)
driver.find_element(By.XPATH, "/html/body/div[1]/div/div/main/div/div[2]/div[2]/div/div[2]/div[5]/div[2]/div[30]/div/div/div/div[3]/input").send_keys("\b1")
driver.find_element(By.XPATH, "//span[text()=' Rezerwuj ']").click()
sleep(1)
driver.find_element(By.XPATH, "//span[text()=' Zapłać ']").click()
sleep(1)
driver.find_element(By.XPATH, "//div[text()=' Wróć do listy wycieczek ']").click()
sleep(1)
driver.find_element(By.XPATH, "//span[text()=' Moje rezerwacje ']").click()
sleep(1)
driver.find_element(By.XPATH, "//div[text()=' Wróć do listy wycieczek ']").click()
sleep(1)
driver.find_element(By.XPATH, "//span[text()=' Statystyki ']").click()
sleep(1)
driver.find_element(By.XPATH, "//div[text()=' Wróć do listy wycieczek ']").click()
sleep(3)

print("All tests passed!")

# Close the driver
driver.quit()